Project: 2048

My goal by the end of the quarter is to have a working 2048 game.
I plan to develop it in Qt. I would like it to be very similar to
the online version, where the program takes keyboard inputs and the
grid elements are color coordinated. The basic version of the game
will likely use buttons in Qt representing the arrow keys, and the
number displays will not be color coordinated as nicely and the numbers
will not have the illusion of the numbers “sliding” since all operations
will take place immediately upon button/keyboard input.

My original goal was to make the game snake, but I had a hard time
figuring out how to implement game ticks into Qt. If time allows,
I want to implement game ticks into my game. For example, one of the
arrow keys might be randomly pressed after a certain amount of inactive
 game ticks to incentivize the player to make moves more quickly.

I will try to implement as many concepts from lecture as possible.
I imagine that I will create classes that inherit from other Objects.
I will try to use generic algorithms and STL containers to handle my
game data as much as possible. As I finish the project, I will try to
use it as a way to study for the final as well, so I will try to implement
as many different concepts from throughout the quarter.

Current to-do list:
- Find ways to add concepts from lecture
- Make it so that a new tile is not added if no shift occurs upon button input
- Make tiles look nicer
- Make the lost() function make the player lose when there are no more possible
  moves instead of when the board is filled, since there could be more moves
- Make the program take keyboard inputs instead of button inputs